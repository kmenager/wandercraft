package io.github.kmenager.data.model

typealias Towns = ArrayList<Town>

data class Town(
    val code: String,
    val name: String,
    val population: Long? = null
)
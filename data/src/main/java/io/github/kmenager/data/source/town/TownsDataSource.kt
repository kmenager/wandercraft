package io.github.kmenager.data.source.town

import io.github.kmenager.data.model.Department
import io.github.kmenager.data.model.DepartmentWithTowns
import io.github.kmenager.data.model.Town
import io.github.kmenager.data.model.Towns
import java.util.*


interface TownsDataSource {

    suspend fun getDepartmentWithTowns(code: String): Result<DepartmentWithTowns?>

    suspend fun getTownsForDepartment(code: String): Result<Towns>

    suspend fun saveTownsAndDepartment(towns: ArrayList<Town>, department: Department)
}
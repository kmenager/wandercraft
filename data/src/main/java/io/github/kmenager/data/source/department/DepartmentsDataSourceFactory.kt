package io.github.kmenager.data.source.department


open class DepartmentsDataSourceFactory(
    private val departmentsDataSource: DepartmentsDataSource
) {

    open fun retrieveDataSource(): DepartmentsDataSource {
        return departmentsDataSource
    }
}
package io.github.kmenager.data.model


typealias Departments = ArrayList<Department>

data class Department(
    val code: String,
    val name: String,
    val region: Region
)
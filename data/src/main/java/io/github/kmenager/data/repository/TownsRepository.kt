package io.github.kmenager.data.repository

import io.github.kmenager.data.model.Department
import io.github.kmenager.data.model.DepartmentWithTowns


interface TownsRepository {

    suspend fun getDepartmentWithTowns(department: Department): Result<DepartmentWithTowns>
}
package io.github.kmenager.data.source.town


open class TownsDataSourceFactory(
    private val townsCacheDataSource: TownsDataSource,
    private val townsRemoteDataSource: TownsDataSource
) {

    /**
     * Return an instance of the Cache Data Store
     */
    open fun retrieveCacheDataSource(): TownsDataSource {
        return townsCacheDataSource
    }

    /**
     * Return an instance of the Remote Data Source
     */
    open fun retrieveRemoteDataSource(): TownsDataSource {
        return townsRemoteDataSource
    }
}
package io.github.kmenager.data

import io.github.kmenager.data.repository.DepartmentsRepository
import io.github.kmenager.data.source.department.DepartmentsDataSourceFactory


class DepartmentDataRepository(
    private val factory: DepartmentsDataSourceFactory
) : DepartmentsRepository {

    override suspend fun getDepartments() = factory.retrieveDataSource().getDepartments()
}
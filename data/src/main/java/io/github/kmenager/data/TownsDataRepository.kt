package io.github.kmenager.data

import io.github.kmenager.data.model.Department
import io.github.kmenager.data.model.DepartmentWithTowns
import io.github.kmenager.data.model.Towns
import io.github.kmenager.data.repository.TownsRepository
import io.github.kmenager.data.source.town.TownsDataSourceFactory


class TownsDataRepository(private val factory: TownsDataSourceFactory) : TownsRepository {

    override suspend fun getDepartmentWithTowns(department: Department): Result<DepartmentWithTowns> {
        val result = factory.retrieveCacheDataSource().getDepartmentWithTowns(department.code)
        var departmentWithTowns = result.getOrNull()
        if (departmentWithTowns == null) {
            val resultTowns =
                factory.retrieveRemoteDataSource().getTownsForDepartment(department.code)
            if (resultTowns.isSuccess) {
                val towns = resultTowns.getOrDefault(
                    Towns()
                )
                factory.retrieveCacheDataSource()
                    .saveTownsAndDepartment(towns, department)

                departmentWithTowns = DepartmentWithTowns(department, towns)
            }
        }

        return if (departmentWithTowns != null) {
            Result.success(departmentWithTowns)
        } else {
            Result.failure(RuntimeException())
        }
    }
}
package io.github.kmenager.data.model


data class DepartmentWithTowns(
    val department: Department,
    val towns: List<Town>
)
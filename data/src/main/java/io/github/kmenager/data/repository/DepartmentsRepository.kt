package io.github.kmenager.data.repository

import io.github.kmenager.data.model.Departments


interface DepartmentsRepository {

    suspend fun getDepartments(): Result<Departments>
}
package io.github.kmenager.data.source.department

import io.github.kmenager.data.model.Departments


interface DepartmentsDataSource {

    suspend fun getDepartments(): Result<Departments>
}
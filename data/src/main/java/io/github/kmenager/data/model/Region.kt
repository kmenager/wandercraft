package io.github.kmenager.data.model


data class Region(
    val code: String,
    val name: String
)
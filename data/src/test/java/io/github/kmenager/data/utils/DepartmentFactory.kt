package io.github.kmenager.data.utils

import io.github.kmenager.commontest.Factory.randomUuid
import io.github.kmenager.data.model.Department
import io.github.kmenager.data.utils.RegionFactory.makeRegion


object DepartmentFactory {

    fun makeDepartment(): Department {
        return Department(
            randomUuid(),
            randomUuid(),
            makeRegion()
        )
    }

    fun makeDepartments(count: Int): List<Department> {
        val departments = arrayListOf<Department>()
        repeat(count) {
            departments.add(makeDepartment())
        }
        return departments
    }
}
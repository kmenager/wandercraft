package io.github.kmenager.data

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import io.github.kmenager.commontest.MainCoroutineRules
import io.github.kmenager.data.model.Departments
import io.github.kmenager.data.source.department.DepartmentsDataSource
import io.github.kmenager.data.source.department.DepartmentsDataSourceFactory
import io.github.kmenager.data.utils.DepartmentFactory
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import kotlin.test.assertEquals
import kotlin.test.assertTrue

@ExperimentalCoroutinesApi
@RunWith(JUnit4::class)
class DepartmentDataRepositoryTest {
    private val departmentsDataSourceFactory = mock<DepartmentsDataSourceFactory>()
    private val remoteDataSource = mock<DepartmentsDataSource>()

    private val departmentDataRepository = DepartmentDataRepository(departmentsDataSourceFactory)

    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRules()

    @Before
    fun setUp() {
        stubDepartmentsDataSourceFactoryRetrieveDataSource()
    }

    @Test
    fun getDepartmentsSuccess() = runBlocking {
        val departments = DepartmentFactory.makeDepartments(3) as Departments

        whenever(remoteDataSource.getDepartments()).thenReturn(Result.success(departments))

        val result = departmentDataRepository.getDepartments()
        assertTrue {
            result.isSuccess
        }

        assertEquals(departments, result.getOrNull())
    }

    @Test
    fun getDepartmentsFailed() = runBlocking {
        val exception = Exception()
        whenever(remoteDataSource.getDepartments()).thenReturn(Result.failure(exception))

        val result = departmentDataRepository.getDepartments()
        assertTrue {
            result.isFailure
        }

        assertEquals(exception, result.exceptionOrNull())
    }


    private fun stubDepartmentsDataSourceFactoryRetrieveDataSource() {
        whenever(departmentsDataSourceFactory.retrieveDataSource())
            .thenReturn(remoteDataSource)
    }

}
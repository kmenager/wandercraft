package io.github.kmenager.data

import com.nhaarman.mockitokotlin2.*
import io.github.kmenager.commontest.MainCoroutineRules
import io.github.kmenager.data.model.DepartmentWithTowns
import io.github.kmenager.data.model.Towns
import io.github.kmenager.data.source.town.TownsDataSource
import io.github.kmenager.data.source.town.TownsDataSourceFactory
import io.github.kmenager.data.utils.DepartmentFactory
import io.github.kmenager.data.utils.TownsFactory
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.ArgumentMatchers.anyString
import kotlin.test.assertEquals
import kotlin.test.assertTrue

@ExperimentalCoroutinesApi
@RunWith(JUnit4::class)
class TownsDataRepositoryTest {

    private val townsDataSourceFactory = mock<TownsDataSourceFactory>()
    private val townsCacheDataSource = mock<TownsDataSource>()
    private val townsRemoteDataSource = mock<TownsDataSource>()

    private val townsDataRepository = TownsDataRepository(townsDataSourceFactory)

    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRules()

    @Before
    fun setUp() {
        stubTownsDataSourceFactoryRetrieveCacheDataSource()
        stubTownsDataSourceFactoryRetrieveRemoteDataSource()
    }

    @Test
    fun getDepartmentWithTownsFromCachedSuccess() = runBlocking {
        val department = DepartmentFactory.makeDepartment()
        val towns = TownsFactory.makeTowns(3)
        val departmentWithTowns = DepartmentWithTowns(department, towns)

        whenever(townsCacheDataSource.getDepartmentWithTowns(anyString())).thenReturn(
            Result.success(
                departmentWithTowns
            )
        )

        val result = townsDataRepository.getDepartmentWithTowns(department)
        assertTrue {
            result.isSuccess
        }
        verify(townsRemoteDataSource, never()).getTownsForDepartment(anyString())
        verify(townsCacheDataSource).getDepartmentWithTowns(department.code)
        assertEquals(departmentWithTowns, result.getOrNull())
    }

    @Test
    fun getDepartmentWithTownsFromCachedEmpty() = runBlocking {
        val department = DepartmentFactory.makeDepartment()
        val towns = TownsFactory.makeTowns(3)
        val departmentWithTowns = DepartmentWithTowns(department, towns)

        whenever(townsCacheDataSource.getDepartmentWithTowns(anyString())).thenReturn(
            Result.success(
                null
            )
        )
        whenever(townsRemoteDataSource.getTownsForDepartment(anyString())).thenReturn(
            Result.success(
                towns as Towns
            )
        )

        val result = townsDataRepository.getDepartmentWithTowns(department)
        assertTrue {
            result.isSuccess
        }
        verify(townsRemoteDataSource).getTownsForDepartment(department.code)
        verify(townsCacheDataSource).getDepartmentWithTowns(department.code)
        verify(townsCacheDataSource).saveTownsAndDepartment(towns, department)
        assertEquals(departmentWithTowns, result.getOrNull())
    }

    @Test
    fun getDepartmentWithTownsRemoteFailed() = runBlocking {
        val department = DepartmentFactory.makeDepartment()

        val exception = Exception()
        whenever(townsCacheDataSource.getDepartmentWithTowns(anyString())).thenReturn(
            Result.success(
                null
            )
        )
        whenever(townsRemoteDataSource.getTownsForDepartment(anyString())).thenReturn(
            Result.failure(
                exception
            )
        )

        val result = townsDataRepository.getDepartmentWithTowns(department)
        assertTrue {
            result.isFailure
        }
        assertTrue { result.exceptionOrNull() is RuntimeException }
        verify(townsCacheDataSource, never()).saveTownsAndDepartment(any(), any())
        assertTrue { true }
    }

    private fun stubTownsDataSourceFactoryRetrieveCacheDataSource() {
        whenever(townsDataSourceFactory.retrieveCacheDataSource())
            .thenReturn(townsCacheDataSource)
    }

    private fun stubTownsDataSourceFactoryRetrieveRemoteDataSource() {
        whenever(townsDataSourceFactory.retrieveRemoteDataSource())
            .thenReturn(townsRemoteDataSource)
    }
}
package io.github.kmenager.data.utils

import io.github.kmenager.commontest.Factory.randomUuid
import io.github.kmenager.data.model.Region


object RegionFactory {

    fun makeRegion(): Region {
        return Region(
            randomUuid(),
            randomUuid()
        )
    }
}
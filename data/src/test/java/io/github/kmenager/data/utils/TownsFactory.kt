package io.github.kmenager.data.utils

import io.github.kmenager.commontest.Factory.randomInt
import io.github.kmenager.commontest.Factory.randomUuid
import io.github.kmenager.data.model.Town


object TownsFactory {

    fun makeTown(): Town {
        return Town(
            randomUuid(),
            randomUuid(),
            randomInt().toLong()
        )
    }

    fun makeTowns(count: Int): List<Town> {
        val towns = arrayListOf<Town>()
        repeat(count) {
            towns.add(makeTown())
        }
        return towns
    }
}
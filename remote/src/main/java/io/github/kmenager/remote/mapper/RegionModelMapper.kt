package io.github.kmenager.remote.mapper

import io.github.kmenager.data.model.Region
import io.github.kmenager.remote.model.RegionModel


class RegionModelMapper : EntityMapper<RegionModel, Region> {
    override fun mapFromRemote(type: RegionModel): Region {
        return Region(
            type.code,
            type.nom
        )
    }
}
package io.github.kmenager.remote.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RegionModel(
    @field:Json(name = "code") val code: String,
    @field:Json(name = "nom") val nom: String
)



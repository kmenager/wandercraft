package io.github.kmenager.remote.mapper

import io.github.kmenager.data.model.Town
import io.github.kmenager.remote.model.TownModel


class TownModelMapper : EntityMapper<TownModel, Town> {
    override fun mapFromRemote(type: TownModel): Town {
        return Town(
            type.code,
            type.nom,
            type.population
        )
    }
}
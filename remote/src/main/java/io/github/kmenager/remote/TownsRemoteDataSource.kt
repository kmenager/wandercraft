package io.github.kmenager.remote

import io.github.kmenager.data.model.Department
import io.github.kmenager.data.model.DepartmentWithTowns
import io.github.kmenager.data.model.Town
import io.github.kmenager.data.model.Towns
import io.github.kmenager.data.source.town.TownsDataSource
import io.github.kmenager.remote.mapper.TownModelMapper
import io.github.kmenager.remote.model.TownModel
import retrofit2.Response
import java.util.*


class TownsRemoteDataSource(
    private val service: ApiService,
    private val townModelMapper: TownModelMapper
) : TownsDataSource {

    override suspend fun getTownsForDepartment(code: String): Result<Towns> {
        val query = "population"
        return try {
            val response = service.getTownsForDepartment(code, query)
            handleResponse(response)
        } catch (e: Exception) {
            Result.failure(e)
        }
    }

    private fun handleResponse(response: Response<List<TownModel>>): Result<Towns> {
        return if (response.isSuccessful) {
            val data = response.body()
            if (data != null) {
                val list = data.map { townModelMapper.mapFromRemote(it) }
                Result.success(Towns(list))
            } else {
                Result.success(Towns())
            }
        } else {
            Result.failure(RuntimeException())
        }
    }

    override suspend fun getDepartmentWithTowns(code: String): Result<DepartmentWithTowns?> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    override suspend fun saveTownsAndDepartment(towns: ArrayList<Town>, department: Department) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
package io.github.kmenager.remote.mapper

import io.github.kmenager.data.model.Department
import io.github.kmenager.remote.model.DepartmentModel

class DepartmentModelMapper(
    private val regionModelMapper: RegionModelMapper
) : EntityMapper<DepartmentModel, Department> {
    override fun mapFromRemote(type: DepartmentModel): Department {
        val region = regionModelMapper.mapFromRemote(type.region)
        return Department(
            type.code,
            type.nom,
            region
        )
    }
}
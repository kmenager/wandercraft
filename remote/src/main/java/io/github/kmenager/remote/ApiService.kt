package io.github.kmenager.remote

import io.github.kmenager.remote.model.DepartmentModel
import io.github.kmenager.remote.model.TownModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface ApiService {

    @GET("departements")
    suspend fun getDepartments(@Query("fields") fields: String): Response<List<DepartmentModel>>

    @GET("departements/{code}/communes")
    suspend fun getTownsForDepartment(@Path("code") code: String, @Query("fields") fields: String): Response<List<TownModel>>

}
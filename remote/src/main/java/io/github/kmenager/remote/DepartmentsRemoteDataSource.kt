package io.github.kmenager.remote

import io.github.kmenager.data.model.Departments
import io.github.kmenager.data.source.department.DepartmentsDataSource
import io.github.kmenager.remote.mapper.DepartmentModelMapper
import io.github.kmenager.remote.model.DepartmentModel
import retrofit2.Response


class DepartmentsRemoteDataSource(
    private val service: ApiService,
    private val departmentModelMapper: DepartmentModelMapper
) : DepartmentsDataSource {

    override suspend fun getDepartments(): Result<Departments> {
        val query = "name,region,code"
        return try {
            val response = service.getDepartments(query)
            handleResponse(response)
        } catch (e: Exception) {
            Result.failure(e)
        }
    }

    private fun handleResponse(response: Response<List<DepartmentModel>>): Result<Departments> {
        return if (response.isSuccessful) {
            val data = response.body()
            if (data != null) {
                val list = data.map { departmentModelMapper.mapFromRemote(it) }
                Result.success(Departments(list))
            } else {
                Result.success(Departments())
            }
        } else {
            Result.failure(RuntimeException())
        }
    }
}
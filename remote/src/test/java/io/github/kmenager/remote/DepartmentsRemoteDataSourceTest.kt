package io.github.kmenager.remote

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import io.github.kmenager.commontest.Factory
import io.github.kmenager.data.model.Departments
import io.github.kmenager.remote.mapper.DepartmentModelMapper
import io.github.kmenager.remote.mapper.RegionModelMapper
import io.github.kmenager.remote.model.DepartmentModel
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.ArgumentMatchers.anyString
import retrofit2.Response
import kotlin.test.assertEquals


@RunWith(JUnit4::class)
class DepartmentsRemoteDataSourceTest {

    private val service = mock<ApiService>()
    private val moshi = Moshi.Builder().build()
    private lateinit var dataSource: DepartmentsRemoteDataSource
    private val departmentModelMapper = DepartmentModelMapper(
        RegionModelMapper()
    )


    @Before
    fun setup() {
        dataSource = DepartmentsRemoteDataSource(service, departmentModelMapper)
    }

    @Test
    fun getDepartmentsListSuccess() = runBlocking {
        val departmentsJson = Factory.getDepartmentsJson()
        val type = Types.newParameterizedType(List::class.java, DepartmentModel::class.java)
        val adapter = moshi.adapter<List<DepartmentModel>>(type)
        val departments = adapter.fromJson(departmentsJson)
        val expected = departments!!.map { departmentModelMapper.mapFromRemote(it) }
        whenever(service.getDepartments(anyString()))
            .thenReturn(Response.success(departments))

        val result = dataSource.getDepartments()
        assertEquals(Result.success(expected), result)
    }

    @Test
    fun getDepartmentsListSuccessWithNoBody() = runBlocking {
        whenever(service.getDepartments(anyString()))
            .thenReturn(Response.success(null))

        val expected = Result.success(Departments())
        val result = dataSource.getDepartments()

        assertEquals(expected, result)
    }

    @Test
    fun getDepartmentsListException() = runBlocking {
        val exception = RuntimeException()
        whenever(service.getDepartments(anyString()))
            .thenThrow(exception)

        val result = dataSource.getDepartments()

        assertEquals(Result.failure(exception), result)
    }

}
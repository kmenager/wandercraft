package io.github.kmenager.remote

import com.nhaarman.mockitokotlin2.eq
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import io.github.kmenager.commontest.Factory
import io.github.kmenager.data.model.Towns
import io.github.kmenager.remote.mapper.TownModelMapper
import io.github.kmenager.remote.model.TownModel
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.ArgumentMatchers.anyString
import retrofit2.Response
import kotlin.test.assertEquals


@RunWith(JUnit4::class)
class TownsRemoteDataSourceTest {

    private val service = mock<ApiService>()
    private val moshi = Moshi.Builder().build()
    private lateinit var dataSource: TownsRemoteDataSource
    private val townModelMapper = TownModelMapper()

    @Before
    fun setup() {
        dataSource = TownsRemoteDataSource(service, townModelMapper)
    }

    @Test
    fun getTownsListSuccess() = runBlocking {
        val townsJson = Factory.getTownsJson()
        val type = Types.newParameterizedType(List::class.java, TownModel::class.java)
        val adapter = moshi.adapter<List<TownModel>>(type)
        val towns = adapter.fromJson(townsJson)
        val expected = towns!!.map { TownModelMapper().mapFromRemote(it) }
        whenever(service.getTownsForDepartment(anyString(), anyString()))
            .thenReturn(Response.success(towns))

        val result = dataSource.getTownsForDepartment("28")
        verify(service).getTownsForDepartment(eq("28"), anyString())
        assertEquals(Result.success(expected), result)
    }

    @Test
    fun getTownsListSuccessWithNoBody() = runBlocking {
        whenever(service.getTownsForDepartment(anyString(), anyString()))
            .thenReturn(Response.success(null))

        val expected = Result.success(Towns())
        val result = dataSource.getTownsForDepartment("28")

        assertEquals(expected, result)
    }

    @Test
    fun getTownsListException() = runBlocking {
        val exception = RuntimeException()
        whenever(service.getTownsForDepartment(anyString(), anyString()))
            .thenThrow(exception)

        val result = dataSource.getTownsForDepartment("28")

        assertEquals(Result.failure(exception), result)
    }

}
package io.github.kmenager.cache.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
    tableName = "town",
    foreignKeys = [ForeignKey(
        entity = DepartmentModel::class,
        parentColumns = arrayOf("code"),
        childColumns = arrayOf("department_code"),
        onDelete = ForeignKey.NO_ACTION
    )]
)
data class TownModel(
    @PrimaryKey
    val code: String,
    @ColumnInfo(name = "department_code")
    val departmentCode: String,
    val name: String,
    val population: Long? = null
)
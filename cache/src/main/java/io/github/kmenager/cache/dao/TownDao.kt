package io.github.kmenager.cache.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import io.github.kmenager.cache.model.TownModel


@Dao
abstract class TownDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertTown(townModel: TownModel)
}
package io.github.kmenager.cache.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import io.github.kmenager.cache.dao.DepartmentDao
import io.github.kmenager.cache.dao.RegionDao
import io.github.kmenager.cache.dao.TownDao
import io.github.kmenager.cache.model.DepartmentModel
import io.github.kmenager.cache.model.RegionModel
import io.github.kmenager.cache.model.TownModel


@Database(
    entities = [DepartmentModel::class, RegionModel::class, TownModel::class],
    version = 1,
    exportSchema = false
)
abstract class WandercraftDatabase : RoomDatabase() {

    abstract fun departmentDao(): DepartmentDao

    abstract fun regionDao(): RegionDao

    abstract fun townDao(): TownDao

    companion object {
        private var INSTANCE: WandercraftDatabase? = null

        private val sLock = Any()

        @JvmStatic
        fun getInstance(context: Context): WandercraftDatabase {
            if (INSTANCE == null) {
                synchronized(sLock) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(
                            context.applicationContext,
                            WandercraftDatabase::class.java, "wandercraft.db"
                        )
                            .build()
                    }
                    return INSTANCE!!
                }
            }
            return INSTANCE!!
        }
    }

}
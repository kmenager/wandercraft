package io.github.kmenager.cache.dao

import androidx.room.*
import io.github.kmenager.cache.dto.DepartmentWithTownsModel
import io.github.kmenager.cache.model.DepartmentModel

@Dao
abstract class DepartmentDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertDepartment(departmentModel: DepartmentModel)

    @Transaction
    @Query("SELECT * FROM department WHERE code = :code")
    abstract suspend fun findByDepartment(code: String): DepartmentWithTownsModel?
}
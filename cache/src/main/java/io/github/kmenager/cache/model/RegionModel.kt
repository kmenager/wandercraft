package io.github.kmenager.cache.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "region")
data class RegionModel(
    @PrimaryKey
    val code: String,
    val name: String
)
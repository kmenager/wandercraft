package io.github.kmenager.cache.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
    tableName = "department",
    foreignKeys = [ForeignKey(
        entity = RegionModel::class,
        parentColumns = arrayOf("code"),
        childColumns = arrayOf("region_code"),
        onDelete = ForeignKey.NO_ACTION
    )]
)
data class DepartmentModel(
    @PrimaryKey
    val code: String,
    @ColumnInfo(name = "region_code")
    val regionCode: String,
    val name: String
)
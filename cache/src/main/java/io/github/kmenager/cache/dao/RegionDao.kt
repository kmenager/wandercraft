package io.github.kmenager.cache.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import io.github.kmenager.cache.model.RegionModel

@Dao
abstract class RegionDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun insertRegion(regionModel: RegionModel)
}
package io.github.kmenager.cache

import io.github.kmenager.cache.db.WandercraftDatabase
import io.github.kmenager.cache.ext.toDepartmentModel
import io.github.kmenager.cache.ext.toDepartmentWithTowns
import io.github.kmenager.cache.ext.toRegionModel
import io.github.kmenager.cache.ext.toTownModelWith
import io.github.kmenager.data.model.Department
import io.github.kmenager.data.model.DepartmentWithTowns
import io.github.kmenager.data.model.Town
import io.github.kmenager.data.model.Towns
import io.github.kmenager.data.source.town.TownsDataSource
import java.util.*


class TownCacheDataSource(
    private val database: WandercraftDatabase
) : TownsDataSource {

    override suspend fun getDepartmentWithTowns(code: String): Result<DepartmentWithTowns?> {
        val departmentWithTownsModel = database.departmentDao().findByDepartment(code)
        val departmentWithTowns = departmentWithTownsModel?.let {
            return@let it.toDepartmentWithTowns()
        }

        return Result.success(departmentWithTowns)
    }

    override suspend fun getTownsForDepartment(code: String): Result<Towns> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override suspend fun saveTownsAndDepartment(towns: ArrayList<Town>, department: Department) {
        database.regionDao().insertRegion(department.region.toRegionModel())
        database.departmentDao().insertDepartment(department.toDepartmentModel())
        towns.forEach {
            database.townDao().insertTown(it.toTownModelWith(department.code))
        }
    }
}
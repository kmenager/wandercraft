package io.github.kmenager.cache.dto

import androidx.room.Embedded
import androidx.room.Relation
import io.github.kmenager.cache.model.DepartmentModel
import io.github.kmenager.cache.model.RegionModel
import io.github.kmenager.cache.model.TownModel


class DepartmentWithTownsModel(
    @Embedded
    val department: DepartmentModel,

    @Relation(parentColumn = "region_code", entityColumn = "code")
    val region: RegionModel,

    @Relation(parentColumn = "code", entityColumn = "department_code", entity = TownModel::class)
    val towns: List<TownModel>
)
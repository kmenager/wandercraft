package io.github.kmenager.cache.ext

import io.github.kmenager.cache.dto.DepartmentWithTownsModel
import io.github.kmenager.cache.model.DepartmentModel
import io.github.kmenager.cache.model.RegionModel
import io.github.kmenager.cache.model.TownModel
import io.github.kmenager.data.model.Department
import io.github.kmenager.data.model.DepartmentWithTowns
import io.github.kmenager.data.model.Region
import io.github.kmenager.data.model.Town

fun Region.toRegionModel(): RegionModel {
    return RegionModel(this.code, this.name)
}

fun RegionModel.toRegion(): Region {
    return Region(this.code, this.name)
}

fun Department.toDepartmentModel(): DepartmentModel {
    return DepartmentModel(this.code, this.region.code, this.name)
}

fun DepartmentModel.toDepartment(region: Region): Department {
    return Department(this.code, this.name, region)
}

fun Town.toTownModelWith(departmentCode: String): TownModel {
    return TownModel(this.code, departmentCode, this.name, this.population)
}

fun TownModel.toTown(): Town {
    return Town(this.code, this.name, this.population)
}

fun DepartmentWithTownsModel.toDepartmentWithTowns(): DepartmentWithTowns {
    val region = this.region.toRegion()
    val towns = this.towns.map { it.toTown() }
    return DepartmentWithTowns(this.department.toDepartment(region), towns)
}
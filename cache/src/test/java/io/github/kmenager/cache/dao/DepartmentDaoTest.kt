package io.github.kmenager.cache.dao

import androidx.room.Room
import io.github.kmenager.cache.db.WandercraftDatabase
import io.github.kmenager.cache.model.DepartmentModel
import io.github.kmenager.cache.model.RegionModel
import io.github.kmenager.cache.model.TownModel
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

@RunWith(RobolectricTestRunner::class)
class DepartmentDaoTest {
    private lateinit var database: WandercraftDatabase

    @Before
    fun initDb() {
        database = Room.inMemoryDatabaseBuilder(
            RuntimeEnvironment.application.baseContext,
            WandercraftDatabase::class.java
        )
            .allowMainThreadQueries()
            .build()
    }

    @After
    fun closeDb() {
        database.close()
    }

    @Test
    fun insertDepartmentSavesData() = runBlocking {
        val regionModel = RegionModel("24", "Centre-Val de Loire")
        database.regionDao().insertRegion(regionModel)
        val departmentModel = DepartmentModel("28", regionModel.code, "Eure-et-Loir")
        database.departmentDao().insertDepartment(departmentModel)
        val townModel = TownModel("28001", departmentModel.code, "Abondant", 2358)
        database.townDao().insertTown(townModel)
        val result = database.departmentDao().findByDepartment("28")
        assertNotNull(result)
        assertEquals(regionModel, result.region)
        assertEquals(departmentModel, result.department)
        assertEquals(townModel, result.towns[0])
    }
}
package io.github.kmenager.wandercraft.ext

import io.github.kmenager.data.model.Department
import io.github.kmenager.data.model.Region
import io.github.kmenager.wandercraft.department.model.DepartmentDto
import io.github.kmenager.wandercraft.department.model.RegionDto

fun Region.toDto() = RegionDto(this.code, this.name)
fun RegionDto.toModel() = Region(this.code, this.name)

fun Department.toDto() = DepartmentDto(
    this.code,
    this.name,
    this.region.toDto()
)

fun DepartmentDto.toModel() = Department(
    this.code,
    this.name,
    this.region.toModel()
)
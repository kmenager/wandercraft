package io.github.kmenager.wandercraft.di

import io.github.kmenager.cache.TownCacheDataSource
import io.github.kmenager.cache.db.WandercraftDatabase
import io.github.kmenager.data.DepartmentDataRepository
import io.github.kmenager.data.TownsDataRepository
import io.github.kmenager.data.repository.DepartmentsRepository
import io.github.kmenager.data.repository.TownsRepository
import io.github.kmenager.data.source.department.DepartmentsDataSource
import io.github.kmenager.data.source.department.DepartmentsDataSourceFactory
import io.github.kmenager.data.source.town.TownsDataSource
import io.github.kmenager.data.source.town.TownsDataSourceFactory
import io.github.kmenager.remote.DepartmentsRemoteDataSource
import io.github.kmenager.remote.TownsRemoteDataSource
import io.github.kmenager.remote.WandercraftServiceFactory
import io.github.kmenager.remote.mapper.DepartmentModelMapper
import io.github.kmenager.remote.mapper.RegionModelMapper
import io.github.kmenager.remote.mapper.TownModelMapper
import io.github.kmenager.wandercraft.BuildConfig
import io.github.kmenager.wandercraft.department.detail.DepartmentDetailViewModel
import io.github.kmenager.wandercraft.department.listing.DepartmentListingViewModel
import io.github.kmenager.wandercraft.department.model.DepartmentDto
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module


val applicationModule = module(override = true) {


    single { WandercraftDatabase.getInstance(androidApplication()) }

    factory<DepartmentsDataSource> { DepartmentsRemoteDataSource(get(), get()) }
    factory { DepartmentsDataSourceFactory(get()) }
    factory<DepartmentsRepository> { DepartmentDataRepository(get()) }

    factory<TownsDataSource>(named("remote")) { TownsRemoteDataSource(get(), get()) }
    factory<TownsDataSource>(named("cached")) { TownCacheDataSource(get()) }
    factory { TownsDataSourceFactory(get(named("cached")), get(named("remote"))) }
    factory<TownsRepository> { TownsDataRepository(get()) }

    factory { RegionModelMapper() }
    factory { DepartmentModelMapper(get()) }
    factory { TownModelMapper() }

    single { WandercraftServiceFactory.makeApiService(BuildConfig.DEBUG, BuildConfig.ENDPOINT) }

    viewModel { DepartmentListingViewModel(get()) }
    viewModel { DepartmentListingViewModel(get()) }

    viewModel { (department: DepartmentDto) -> DepartmentDetailViewModel(department, get()) }

}
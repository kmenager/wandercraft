package io.github.kmenager.wandercraft.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.TextView
import androidx.annotation.StringRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import io.github.kmenager.wandercraft.R


open class DetailView : ConstraintLayout {

    private lateinit var title: TextView
    private lateinit var description: TextView


    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(context)
    }

    private fun init(context: Context) {
        val view = LayoutInflater.from(context).inflate(R.layout.detail_view, this, true)
        title = view.findViewById(R.id.title)
        description = view.findViewById(R.id.description)
        ViewCompat.setElevation(view, 8f)
        view.setBackgroundColor(ContextCompat.getColor(context, R.color.color_background))
    }

    fun setTitle(title: String) {
        this.title.text = title
    }

    fun setTitle(@StringRes title: Int) {
        this.title.setText(title)
    }

    fun setDescription(description: String) {
        this.description.text = description
    }

    fun setDescription(@StringRes description: Int) {
        this.description.setText(description)
    }
}
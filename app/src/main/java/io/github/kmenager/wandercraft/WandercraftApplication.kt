package io.github.kmenager.wandercraft

import android.app.Application
import io.github.kmenager.wandercraft.di.applicationModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber


class WandercraftApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@WandercraftApplication)
            modules(listOf(applicationModule))
        }

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

    }
}
package io.github.kmenager.wandercraft.department.listing

import androidx.lifecycle.*
import io.github.kmenager.data.model.Departments
import io.github.kmenager.data.repository.DepartmentsRepository
import io.github.kmenager.wandercraft.department.model.DepartmentDto
import io.github.kmenager.wandercraft.ext.toDto
import kotlinx.coroutines.Dispatchers
import timber.log.Timber
import java.util.*


sealed class DepartmentState {
    data class Success(val model: List<DepartmentDto>) : DepartmentState()
    object Loading : DepartmentState()
    object Error : DepartmentState()
}

class DepartmentListingViewModel(
    private val departmentsRepository: DepartmentsRepository
) : ViewModel() {


    private var departmentList: List<DepartmentDto> = emptyList()

    private val initialLoad: LiveData<DepartmentState> = fetchData()

    private val filter = MutableLiveData<String>()
    private val filteredDepartments: LiveData<DepartmentState> = filter.map { input ->
        val newList = departmentList.filter {
            it.name.toLowerCase(Locale.FRANCE).contains(input.toLowerCase(Locale.FRANCE)) or
                    it.code.toLowerCase(Locale.FRANCE).contains(input.toLowerCase(Locale.FRANCE))
        }
        DepartmentState.Success(newList)
    }

    private val retry = MutableLiveData<Unit>()
    private val loadAfterRetry: LiveData<DepartmentState> = retry.switchMap {
        fetchData()
    }

    val state = MediatorLiveData<DepartmentState>()

    init {
        state.addSource(initialLoad) {
            state.value = it
        }

        state.addSource(loadAfterRetry) {
            state.value = it
        }

        state.addSource(filteredDepartments) {
            state.value = it
        }
    }

    fun retry() {
        retry.postValue(Unit)
    }

    fun filter(input: String) {
        filter.postValue(input)
    }

    private fun fetchData(): LiveData<DepartmentState> {
        return liveData(viewModelScope.coroutineContext + Dispatchers.IO) {
            emit(DepartmentState.Loading)

            try {
                val result = departmentsRepository.getDepartments()
                result.fold({ value: Departments ->
                    departmentList = value.map { it.toDto() }
                    emit(DepartmentState.Success(departmentList))
                }, { exception ->
                    Timber.e(exception)
                    emit(DepartmentState.Error)
                })
            } catch (e: Exception) {

                emit(DepartmentState.Error)
            }
        }
    }
}
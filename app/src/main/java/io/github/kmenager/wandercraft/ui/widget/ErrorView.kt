package io.github.kmenager.wandercraft.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.google.android.material.button.MaterialButton
import io.github.kmenager.wandercraft.R


class ErrorView : ConstraintLayout {

    private lateinit var label: TextView
    private lateinit var retryButton: MaterialButton

    var retryAction: (() -> Unit)? = null

    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(context)
    }

    private fun init(context: Context) {
        val view = LayoutInflater.from(context).inflate(R.layout.error_view, this, true)
        label = view.findViewById(R.id.fetch_error_label)
        retryButton = view.findViewById(R.id.fetch_error_button)

        retryButton.setOnClickListener {
            retryAction?.invoke()
        }
    }
}
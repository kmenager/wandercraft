package io.github.kmenager.wandercraft.department.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DepartmentDto(
    val code: String,
    val name: String,
    val region: RegionDto
) : Parcelable {

    val completeName: String
        get() = "$code - $name"
}
package io.github.kmenager.wandercraft.ui.widget

import android.content.Context
import android.util.AttributeSet
import io.github.kmenager.wandercraft.R
import io.github.kmenager.wandercraft.department.model.DepartmentDto


class DepartmentDetailView : DetailView {

    var department: DepartmentDto? = null
        set(value) {
            value?.let {
                setTitle(R.string.department_title)
                setDescription(it.completeName)
            }
            field = value
        }

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

}
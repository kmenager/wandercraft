package io.github.kmenager.wandercraft.ui.widget

import android.content.Context
import android.util.AttributeSet
import io.github.kmenager.wandercraft.R


class TownsNumberDetailView : DetailView {

    var numberOfTowns: Int? = null
        set(value) {
            value?.let {
                setTitle(R.string.towns_number_title)
                setDescription(context.resources.getQuantityString(R.plurals.towns_number, it, it))
            }
            field = value
        }

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

}
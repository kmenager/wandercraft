package io.github.kmenager.wandercraft.department.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RegionDto(
    val code: String,
    val name: String
) : Parcelable
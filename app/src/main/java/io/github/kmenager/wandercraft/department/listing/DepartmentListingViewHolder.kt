package io.github.kmenager.wandercraft.department.listing

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import io.github.kmenager.wandercraft.R
import io.github.kmenager.wandercraft.department.model.DepartmentDto
import kotlinx.android.synthetic.main.item_listing_department.view.listing_department_name as departmentName


class DepartmentListingViewHolder(
    itemView: View
) : RecyclerView.ViewHolder(itemView) {

    fun bind(
        department: DepartmentDto,
        shouldShowBackground: Boolean,
        callback: (DepartmentDto) -> Unit
    ) {
        itemView.departmentName.text = department.completeName
        itemView.setOnClickListener {
            callback(department)
        }
//        if (shouldShowBackground) {
//            itemView.setBackgroundColor(
//                ContextCompat.getColor(
//                    itemView.context,
//                    R.color.background_item
//                )
//            )
//        } else {
//            itemView.background = null
//        }
    }

    companion object {
        @JvmStatic
        fun createRecipeViewHolder(parent: ViewGroup): DepartmentListingViewHolder {
            return DepartmentListingViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_listing_department,
                    parent,
                    false
                )
            )
        }
    }
}
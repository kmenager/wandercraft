package io.github.kmenager.wandercraft.department.detail

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import io.github.kmenager.wandercraft.R
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import kotlinx.android.synthetic.main.department_detail_fragment.detail_department as departmentView
import kotlinx.android.synthetic.main.department_detail_fragment.detail_error_view as errorView
import kotlinx.android.synthetic.main.department_detail_fragment.detail_population as populationView
import kotlinx.android.synthetic.main.department_detail_fragment.detail_progress_bar as progressBar
import kotlinx.android.synthetic.main.department_detail_fragment.detail_region as regionView
import kotlinx.android.synthetic.main.department_detail_fragment.detail_towns_number as townsView


class DepartmentDetailFragment : Fragment(R.layout.department_detail_fragment) {

    private val args: DepartmentDetailFragmentArgs by navArgs()

    private val viewModel by viewModel<DepartmentDetailViewModel> {
        parametersOf(args.department)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupError()
    }

    private fun setupError() {
        errorView.retryAction = {
            viewModel.retry()
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.state.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            renderDepartmentDetailState(it)
        })
    }

    private fun renderDepartmentDetailState(state: DepartmentDetailState) {
        when (state) {
            DepartmentDetailState.Loading -> {
                progressBar.isVisible = true
                errorView.isVisible = false
            }
            is DepartmentDetailState.Success -> {
                progressBar.isVisible = false
                departmentView.isVisible = true
                regionView.isVisible = true
                populationView.isVisible = true
                townsView.isVisible = true
                departmentView.department = state.model.department
                regionView.region = state.model.department.region
                populationView.population = state.model.population
                townsView.numberOfTowns = state.model.townNumber
            }
            DepartmentDetailState.Error -> {
                progressBar.isVisible = false
                errorView.isVisible = true
            }
        }
    }
}
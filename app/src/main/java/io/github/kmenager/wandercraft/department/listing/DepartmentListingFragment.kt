package io.github.kmenager.wandercraft.department.listing

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import io.github.kmenager.wandercraft.R
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import kotlinx.android.synthetic.main.department_listing_fragment.listing_error_view as errorView
import kotlinx.android.synthetic.main.department_listing_fragment.listing_progress_bar as progressBar
import kotlinx.android.synthetic.main.department_listing_fragment.listing_recycler_view as recyclerView


class DepartmentListingFragment : Fragment(R.layout.department_listing_fragment) {

    private lateinit var departmentAdapter: DepartmentListingAdapter
    private lateinit var searchView: SearchView
    private val viewModel by viewModel<DepartmentListingViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_listing, menu)

        val searchItem = menu.findItem(R.id.action_search)
        searchView = searchItem.actionView as SearchView
        searchView.setIconifiedByDefault(true)

        val searchTextField =
            searchView.findViewById<SearchView.SearchAutoComplete>(androidx.appcompat.R.id.search_src_text)
        searchTextField.setTextColor(
            ContextCompat.getColor(
                requireContext(),
                R.color.color_on_primary
            )
        )
        searchTextField.setHintTextColor(
            ContextCompat.getColor(
                requireContext(),
                R.color.hint_color
            )
        )
        searchView.maxWidth = 10000

        searchView.queryHint = getString(R.string.search_hint)

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                viewModel.filter(newText)
                return true
            }

        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupAdapter()
        setupRecyclerView()
        setupError()
    }

    private fun setupError() {
        errorView.retryAction = {
            viewModel.retry()
        }
    }

    private fun setupAdapter() {
        departmentAdapter = DepartmentListingAdapter {
            Timber.d(it.toString())
            val action =
                DepartmentListingFragmentDirections.actionDepartmentListFragmentToDepartmentDetailFragment(
                    it
                )
            findNavController(this).navigate(action)
        }
    }

    private fun setupRecyclerView() {
        recyclerView.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = departmentAdapter
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.state.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            renderDepartmentState(it)
        })
    }

    private fun renderDepartmentState(state: DepartmentState) {
        when (state) {
            DepartmentState.Loading -> {
                progressBar.isVisible = true
                recyclerView.isVisible = false
                errorView.isVisible = false
            }
            is DepartmentState.Success -> {
                progressBar.isVisible = false
                recyclerView.isVisible = true
                departmentAdapter.submitList(state.model)
            }
            DepartmentState.Error -> {
                progressBar.isVisible = false
                errorView.isVisible = true
            }
        }
    }

}
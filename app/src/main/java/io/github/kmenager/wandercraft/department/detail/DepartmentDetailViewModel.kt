package io.github.kmenager.wandercraft.department.detail

import androidx.lifecycle.*
import io.github.kmenager.data.model.DepartmentWithTowns
import io.github.kmenager.data.repository.TownsRepository
import io.github.kmenager.wandercraft.department.model.DepartmentDetailDto
import io.github.kmenager.wandercraft.department.model.DepartmentDto
import io.github.kmenager.wandercraft.ext.toDto
import io.github.kmenager.wandercraft.ext.toModel
import kotlinx.coroutines.Dispatchers
import timber.log.Timber
import java.text.NumberFormat
import java.util.*

sealed class DepartmentDetailState {
    data class Success(val model: DepartmentDetailDto) : DepartmentDetailState()
    object Loading : DepartmentDetailState()
    object Error : DepartmentDetailState()
}

class DepartmentDetailViewModel(
    private val department: DepartmentDto,
    private val townsRepository: TownsRepository
) : ViewModel() {

    private val retry = MutableLiveData<Unit>()

    private val initialLoad: LiveData<DepartmentDetailState> = fetchData()

    private val loadAfterRetry: LiveData<DepartmentDetailState> = retry.switchMap {
        fetchData()
    }

    val state = MediatorLiveData<DepartmentDetailState>()

    init {
        state.addSource(initialLoad) {
            state.value = it
        }

        state.addSource(loadAfterRetry) {
            state.value = it
        }
    }

    fun retry() {
        retry.postValue(Unit)
    }

    private fun fetchData(): LiveData<DepartmentDetailState> {
        return liveData(viewModelScope.coroutineContext + Dispatchers.IO) {
            emit(DepartmentDetailState.Loading)

            try {
                val result = townsRepository.getDepartmentWithTowns(department.toModel())
                result.fold({ value: DepartmentWithTowns ->
                    val list = value.towns.mapNotNull { it.population }
                    var numberOfPopulation = 0L
                    if (list.isNotEmpty()) {
                        numberOfPopulation = list.reduce { acc, population -> acc + population }
                    }
                    val numberOfTowns = value.towns.size

                    val departmentDetailDto = DepartmentDetailDto(
                        value.department.toDto(),
                        NumberFormat.getInstance(Locale.FRENCH).format(numberOfPopulation),
                        numberOfTowns
                    )
                    emit(DepartmentDetailState.Success(departmentDetailDto))
                }, { exception ->
                    Timber.e(exception)
                    emit(DepartmentDetailState.Error)
                })
            } catch (e: Exception) {
                emit(DepartmentDetailState.Error)
            }
        }
    }
}
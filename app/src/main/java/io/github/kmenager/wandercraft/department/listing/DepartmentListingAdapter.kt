package io.github.kmenager.wandercraft.department.listing

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import io.github.kmenager.wandercraft.department.model.DepartmentDto


class DepartmentListingAdapter(private val callback: (DepartmentDto) -> Unit) :
    ListAdapter<DepartmentDto, DepartmentListingViewHolder>(diffCallback) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DepartmentListingViewHolder {
        return DepartmentListingViewHolder.createRecipeViewHolder(parent)
    }

    override fun onBindViewHolder(holder: DepartmentListingViewHolder, position: Int) {
        holder.bind(getItem(position), position % 2 == 0, callback)
    }

    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<DepartmentDto>() {
            override fun areItemsTheSame(oldItem: DepartmentDto, newItem: DepartmentDto): Boolean =
                oldItem.code == newItem.code

            override fun areContentsTheSame(
                oldItem: DepartmentDto,
                newItem: DepartmentDto
            ): Boolean =
                oldItem == newItem
        }
    }
}
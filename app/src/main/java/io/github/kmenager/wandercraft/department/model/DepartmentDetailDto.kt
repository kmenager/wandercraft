package io.github.kmenager.wandercraft.department.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DepartmentDetailDto(
    val department: DepartmentDto,
    val population: String,
    val townNumber: Int
) : Parcelable
package io.github.kmenager.wandercraft.ui.widget

import android.content.Context
import android.util.AttributeSet
import io.github.kmenager.wandercraft.R


class PopulationDetailView : DetailView {

    var population: String? = null
        set(value) {
            value?.let {
                setTitle(R.string.population_title)
                setDescription(context.getString(R.string.population_detail, it))
            }
            field = value
        }

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

}
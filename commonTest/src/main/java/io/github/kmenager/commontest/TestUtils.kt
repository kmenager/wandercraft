package io.github.kmenager.commontest

import com.squareup.moshi.Moshi
import java.io.BufferedReader
import java.io.InputStreamReader


object TestUtils {

    fun <T> getJsonFromFile(clazz: Class<T>, path: String): String {
        val inputStream = clazz.classLoader.getResourceAsStream(path)
        val responseFile = InputStreamReader(inputStream)

        val reader = BufferedReader(responseFile)
        val sb = StringBuilder()
        var mLine = reader.readLine()
        while (mLine != null) {
            sb.append(mLine)
            mLine = reader.readLine()
        }
        reader.close()
        return sb.toString()
    }

    fun <T> getObjectFromString(moshi: Moshi, json: String, clazz: Class<T>): T {
        val adapter = moshi.adapter(clazz)
        return adapter.fromJson(json)!!
    }

    fun <T> getObjectFromFile(
        moshi: Moshi,
        currentClass: Class<T>,
        path: String,
        returnClazz: Class<T>
    ): T {
        val fileContent = getJsonFromFile(currentClass, path)
        return getObjectFromString(moshi, fileContent, returnClazz)
    }

}
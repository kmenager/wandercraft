package io.github.kmenager.commontest

import kotlin.random.Random


object Factory {

    fun getDepartmentsJson(): String {
        return TestUtils.getJsonFromFile(this::class.java, "GET_departments.json")
    }

    fun getTownsJson(): String {
        return TestUtils.getJsonFromFile(this::class.java, "GET_towns.json")
    }

    fun randomUuid(): String {
        return java.util.UUID.randomUUID().toString()
    }

    fun randomInt(): Int {
        return Random.nextInt()
    }

    fun randomLong(): Long {
        return randomInt().toLong()
    }

    fun randomBoolean(): Boolean {
        return Math.random() < 0.5
    }
}